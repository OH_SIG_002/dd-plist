﻿## dd-plist单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/3breadt/dd-plist/tree/master/src/test/java/com/dd/plist/test) 进行单元测试

**单元测试用例覆盖情况**

### dd-plist

| 接口名                                   | 是否通过 | 备注 |
|---------------------------------------| -------- | ---- |
| PropertyListParser.parseByBytes       | pass     |      |
| PropertyListParser.parse              | pass     |      |
| PropertyListParser.parseByInt8Array   | pass     |      |
| PropertyListParser.parseByPath        | pass     |      |
| PropertyListParser.convertToXmlByFile | pass     |      |
| BinaryPropertyListParser.parseByByte  | pass     |      |
| BinaryPropertyListParser.copyOfRange  | pass     |      |
| NSArray.objectAtIndex                 | pass     |      |
| NSArray.remove                        | pass     |      |
| NSArray.setValue                      | pass     |      |
| NSArray.containsObject                | pass     |      |
| NSArray.indexOfObject                 | pass     |      |
| NSArray.indexOfIdenticalObject        | pass     |      |
| NSArray.objectsAtIndexes              | pass     |      |
| NSArray.getArray                      | pass     |      |
| NSArray.lastObject                    | pass     |      |
| NSData.getBytes                       | pass     |      |
| NSData.length                         | pass     |      |
| NSData.getBase64EncodedData           | pass     |      |
| NSData.getHashMap                     | pass     |      |
| NSData.objectForKey                   | pass     |      |
| NSDictionary.containsValue            | pass     |      |
| NSDictionary.containsKey              | pass     |      |
| NSDictionary.get                      | pass     |      |
| NSDictionary.getSize                  | pass     |      |
| NSNumber.getType                      | pass     |      |
| NSNumber.isBoolean                    | pass     |      |
| NSNumber.isInteger                    | pass     |      |
| NSNumber.getBoolValue                 | pass     |      |
| NSNumber.getIntValue                  | pass     |      |
| NSNumber.clone                        | pass     |      |
| NSSet.containsObject                  | pass     |      |
| NSSet.addObject                       | pass     |      |
| NSSet.removeObject                    | pass     |      |
| NSSet.member                          | pass     |      |
| NSSet.clone                           | pass     |      |
| NSSet.count                           | pass     |      |
| NSString.intValue                     | pass     |      |
| NSString.floatValue                   | pass     |      |
| NSString.doubleValue                  | pass     |      |
| NSString.boolValue                    | pass     |      |
| NSString.getContent                   | pass     |      |
| UID.getBytes                          | pass     |      |
| XMLPropertyListParser.parse           | pass     |      |
| XMLPropertyListParser.parseByBytes    | pass     |      |
| XMLPropertyListParser.parseByStream   | pass     |      |